FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt upgrade && apt install docker.io -y

COPY entrypoint.sh /bin
RUN ["chmod", "+x", "/bin/entrypoint.sh"]
ENTRYPOINT [ "/bin/entrypoint.sh" ]
